# Mastodon Link Verification

This should just be a "simple" way for the link verification of my <a rel="me" href="https://mastodontech.de/@9Lukas5">Mastodon</a> profile and lead you to my [GitLab](https://gitlab.com/9Lukas5) and [GitHub](https://github.com/9Lukas5) profiles at the same time ;)
