# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0](../../../-/compare/0.0.0...1.0.0) (2022-04-29)


### Features

* let's deploy this ([2556afd](2556afda897eabb9a6f88c6e7f3072be9019c301))
